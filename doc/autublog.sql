/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2018-09-30 20:53:29
*/
create database autublog;
use autublog;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `access_log`
-- ----------------------------
DROP TABLE IF EXISTS `access_log`;
CREATE TABLE `access_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gmtCreate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `referer` varchar(1024) DEFAULT NULL COMMENT '请求来源地址',
  `target` varchar(12024) NOT NULL COMMENT '请求访问地址',
  `ip` varchar(128) DEFAULT NULL COMMENT '来源Ip',
  `cookie` varchar(128) DEFAULT NULL COMMENT '访问者cookie 标识是否是重复访问',
  `info` varchar(2048) DEFAULT NULL COMMENT '详细日志',
  `userAgent` varchar(2048) DEFAULT NULL,
  `agentUserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of access_log
-- ----------------------------

-- ----------------------------
-- Table structure for `agent_user`
-- ----------------------------
DROP TABLE IF EXISTS `agent_user`;
CREATE TABLE `agent_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `gmtCreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `website` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agent_user
-- ----------------------------

-- ----------------------------
-- Table structure for `article`
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `title` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '文章标题',
  `content` longtext COLLATE utf8_bin COMMENT '文章内容',
  `gmtCreate` datetime NOT NULL COMMENT '发表时间',
  `pv` int(11) DEFAULT '1' COMMENT '阅读量',
  `thumbImg` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT '预览图片',
  `state` int(1) DEFAULT '0' COMMENT '0为草稿1为发布2为删除',
  `isTop` tinyint(1) DEFAULT '0' COMMENT '1为置顶文章,0相反',
  `gmtModified` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `identify` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '访问路径',
  `intro` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `allowComment` tinyint(1) DEFAULT '1',
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '第一篇文章', 0x3C6831207374796C653D22666F6E742D66616D696C793A202671756F743B4E6F746F2053616E732671756F743B2C2073616E732D73657269663B20636F6C6F723A2072676228302C20302C2030293B223EE7ACACE4B880E7AF873C2F68313E3C703E266E6273703B20266E6273703B20E4BDA0E5868DE69DA5E58699E782B9E4BB80E4B988E590A73C2F703E, '2018-09-30 19:11:39', '6', '/upload/thumb/jpg/520520_20180930191137.jpg', '1', '0', '2018-09-30 19:11:39', '20180930191139', '第一篇    你再来写点什么吧', null, '1');

-- ----------------------------
-- Table structure for `baidu_seo_config`
-- ----------------------------
DROP TABLE IF EXISTS `baidu_seo_config`;
CREATE TABLE `baidu_seo_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(256) DEFAULT NULL,
  `token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of baidu_seo_config
-- ----------------------------

-- ----------------------------
-- Table structure for `blogroll`
-- ----------------------------
DROP TABLE IF EXISTS `blogroll`;
CREATE TABLE `blogroll` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT '友链id',
  `sort` tinyint(2) NOT NULL DEFAULT '0',
  `title` char(25) COLLATE utf8_bin DEFAULT NULL COMMENT '友链名称',
  `url` char(40) COLLATE utf8_bin DEFAULT NULL COMMENT '友链网址',
  `gmtCreate` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '备注信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_priority` (`sort`) USING BTREE,
  UNIQUE KEY `uk_name` (`title`),
  UNIQUE KEY `uk_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of blogroll
-- ----------------------------
INSERT INTO `blogroll` VALUES ('1', '1', 'Autu博客', 'www.wenhaofan.com', '2018-09-30 19:19:59');

-- ----------------------------
-- Table structure for `code_rel_meta`
-- ----------------------------
DROP TABLE IF EXISTS `code_rel_meta`;
CREATE TABLE `code_rel_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modityCodeId` int(11) DEFAULT NULL COMMENT '商品id',
  `metaId` int(11) DEFAULT NULL COMMENT '元数据id',
  `type` int(11) DEFAULT NULL COMMENT '0为分类,1为标签',
  `gmt_create` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of code_rel_meta
-- ----------------------------

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(2048) DEFAULT NULL,
  `gmtCreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(256) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `identify` varchar(256) DEFAULT NULL,
  `toUserId` int(11) DEFAULT NULL,
  `isAduit` tinyint(2) DEFAULT NULL COMMENT '0为审核未通过,1为审核通过',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keywords` varchar(512) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `author` varchar(256) DEFAULT NULL,
  `ICPRecord` varchar(256) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `gmtCreate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `emailServer` varchar(255) DEFAULT NULL,
  `fromEmail` varchar(255) DEFAULT NULL,
  `emailPassword` varchar(255) DEFAULT NULL,
  `ico` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `qiniuAk` varchar(255) DEFAULT NULL,
  `qiniuSk` varchar(255) DEFAULT NULL,
  `qiniuBucket` varchar(255) DEFAULT NULL,
  `qiniuUrl` varchar(255) DEFAULT NULL,
  `isAuditComment` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', 'Autu个人博客', '一款开源的个人博客', '范文皓', null, 'Autu个人博客', '2018-09-30 19:19:32', null, null, null, '/upload/ico/ico/520520_20180930205220.ico', '/upload/logo/png/520520_20180930205236.png', null, null, null, null, '0');

-- ----------------------------
-- Table structure for `disk`
-- ----------------------------
DROP TABLE IF EXISTS `disk`;
CREATE TABLE `disk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `size` int(10) unsigned DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `parentId` int(11) DEFAULT '0',
  `gmtCreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmtModify` datetime DEFAULT NULL,
  `state` tinyint(2) DEFAULT '0' COMMENT '0为正常1为假删除',
  `thumbUrl` varchar(248) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of disk
-- ----------------------------

-- ----------------------------
-- Table structure for `login_record`
-- ----------------------------
DROP TABLE IF EXISTS `login_record`;
CREATE TABLE `login_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '登录记录id',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '登录时间',
  `userId` int(11) NOT NULL COMMENT '用户id',
  `ip` varchar(30) COLLATE utf8_bin DEFAULT '' COMMENT '登录地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- ----------------------------
-- Table structure for `meta`
-- ----------------------------
DROP TABLE IF EXISTS `meta`;
CREATE TABLE `meta` (
  `mname` varchar(255) DEFAULT NULL COMMENT '名',
  `type` varchar(64) DEFAULT NULL COMMENT '类型',
  `count` tinyint(4) DEFAULT NULL COMMENT '总数',
  `sort` tinyint(4) DEFAULT NULL COMMENT '排序',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of meta
-- ----------------------------
INSERT INTO `meta` VALUES ('默认分类', 'category', null, '1', null, '1');
INSERT INTO `meta` VALUES ('默认标签', 'tag', null, '1', null, '2');

-- ----------------------------
-- Table structure for `metaweblog_config`
-- ----------------------------
DROP TABLE IF EXISTS `metaweblog_config`;
CREATE TABLE `metaweblog_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `website` varchar(256) DEFAULT NULL,
  `userName` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `gmtCreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of metaweblog_config
-- ----------------------------

-- ----------------------------
-- Table structure for `metaweblog_relevance`
-- ----------------------------
DROP TABLE IF EXISTS `metaweblog_relevance`;
CREATE TABLE `metaweblog_relevance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `metaweblogId` int(11) DEFAULT NULL,
  `postId` varchar(256) DEFAULT NULL,
  `articleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of metaweblog_relevance
-- ----------------------------

-- ----------------------------
-- Table structure for `nav`
-- ----------------------------
DROP TABLE IF EXISTS `nav`;
CREATE TABLE `nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nav
-- ----------------------------
INSERT INTO `nav` VALUES ('1', '首页', '/', '99');
INSERT INTO `nav` VALUES ('2', '友链', '/links', '98');
INSERT INTO `nav` VALUES ('3', '关于我', '/about', '97');

-- ----------------------------
-- Table structure for `relevancy`
-- ----------------------------
DROP TABLE IF EXISTS `relevancy`;
CREATE TABLE `relevancy` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mid` int(10) unsigned DEFAULT NULL COMMENT '关联主id',
  `cid` int(10) unsigned DEFAULT NULL COMMENT '内容id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of relevancy
-- ----------------------------

-- ----------------------------
-- Table structure for `session`
-- ----------------------------
DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `expireAt` bigint(20) NOT NULL,
  `userId` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

 
-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `gmtCreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `level` int(2) DEFAULT '0' COMMENT '0为普通日志1为警告日志2为错误日志',
  `url` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `name` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `pwd` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '用户登录密码',
  `age` tinyint(4) NOT NULL DEFAULT '1' COMMENT '用户年龄',
  `gender` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1为男，0为女',
  `email` varchar(30) COLLATE utf8_bin DEFAULT NULL COMMENT '用户邮箱',
  `account` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '登录账号',
  `gmtCreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户注册时间',
  `qq` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `abposition` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `headImg` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `about` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'autu', '96e79218965eb72c92a549dd5a330112', '18', '1', null, 'admin', '2018-09-30 19:07:22', '2195743583', '欢迎各位加入qq群:662586079', 'java开发者', null, '北京', 0x0A0AE6ACA2E8BF8EE59084E4BD8DE58AA0E585A57171E7BEA43A3636323538363037390A0A);
